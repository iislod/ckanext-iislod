from setuptools import setup, find_packages  # Always prefer setuptools over distutils
from codecs import open  # To use a consistent encoding
from os import path


setup(
    name='''ckanext-iislod''',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # http://packaging.python.org/en/latest/tutorial.html#version
    version='0.0.1',

    description='''CKAN Extension for Open Data Web''',

    # The project's main homepage.
    url='https://gitlab.com/iislod/ckanext-iislod',

    # Author details
    author='''Sol Lee''',
    author_email='''u103133.u103135@gmail.com''',

    # Choose your license
    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        # 3 - Alpha
        # 4 - Beta
        # 5 - Production/Stable
        'Development Status :: 4 - Beta',

        # Pick your license as you wish (should match "license" above)
        'License :: MIT',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
    ],


    # What does your project relate to?
    keywords='''CKAN''',

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),

    # List run-time dependencies here.  These will be installed by pip when your
    # project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/technical.html#install-requires-vs-requirements-files
    install_requires=[],

    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    include_package_data=True,
    package_data={
    },

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages.
    # see http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    data_files=[],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points='''
        [ckan.plugins]
        iislod=ckanext.iislod.plugin:IislodPlugin
        dc15_harvester=ckanext.iislod.harvesters:DC15RDFHarvester
        agent_harvester=ckanext.iislod.harvesters:AgentRDFHarvester
        project_harvester=ckanext.iislod.harvesters:ProjectRDFHarvester
        event_harvester=ckanext.iislod.harvesters:EventRDFHarvester
        article_harvester=ckanext.iislod.harvesters:ArticleRDFHarvester
        r1_harvester=ckanext.iislod.harvesters:R1RDFHarvester

        [ckan.rdf.profiles]
        dc15_profile=ckanext.iislod.profiles.dc15:DC15Profile
        agent_profile=ckanext.iislod.profiles.agent:AgentProfile
        project_profile=ckanext.iislod.profiles.project:ProjectProfile
        event_profile=ckanext.iislod.profiles.event:EventProfile
        article_profile=ckanext.iislod.profiles.article:ArticleProfile
        r1_profile=ckanext.iislod.profiles.r1:R1Profile
    ''',
)
