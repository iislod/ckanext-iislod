=================================================
ckanext-iislod - CKAN Extension for Open Data Web
=================================================

Most data.odw.tw specific CKAN customizations are contained within this extension.

---------------
Prerequirements
---------------

- **Data.odw.tw CKAN.** The code powering the Data.odw.tw instance of CKAN.

  - `release-data-odw-tw <https://gitlab.com/iislod/ckan>`_ - The main development branch used for the current data.odw.tw.

- **Extensions.** We have developed/modified several CKAN extensions. The `full list of installed extensions can be seen via the CKAN API <http://data.odw.tw/api/util/status>`_. Custom extensions include:

  - `iislod/ckanext-iislod <https://gitlab.com/iislod/ckanext-iislod>`_ - Most data.odw.tw specific CKAN customizations are contained within this extension.
  - `iislod/ckanext-spatial <https://gitlab.com/iislod/ckanext-spatial>`_ - Geospatial extension for CKAN.
  - `iislod/ckanext-dcat <https://gitlab.com/iislod/ckanext-dcat>`_ - Allow CKAN to expose and consume metadata from other catalogs using RDF documents serialized using DCAT.
  - `iislod/ckanext-repeating <https://gitlab.com/iislod/ckanext-repeating>`_ - Repeating fields for CKAN.
  - `iislod/ckanext-sparql <https://gitlab.com/iislod/ckanext-sparql>`_ - SPARQL interface for CKAN.
  - `iislod/ckanext-tempsearch <https://gitlab.com/iislod/ckanext-tempsearch>`_ - Temporal search for CKAN datasets.

- **Other Extensions.**

  - `ckan/ckanapi <https://github.com/ckan/ckanapi>`_ - A command line interface and Python module for accessing the CKAN Action API.
  - `ckan/ckanext-harvest <https://github.com/ckan/ckanext-harvest>`_ - Remote harvesting extension for CKAN.
  - `open-data/ckanext-scheming <https://github.com/open-data/ckanext-scheming>`_ - Easy, sharable custom CKAN schemas.

- **Libraries for Apache Solr.** Download them to ``YOUR_SOLR_FOLDER/server/solr-webapp/webapp/WEB-INF/lib``.

  - `JTS Topology Suite <https://sourceforge.net/projects/jts-topo-suite/files/jts/1.13/jts-1.13.zip/download>`_ - For geospatial search.
  - `mmseg4j-solr <http://pan.baidu.com/s/1dD7qMFf>`_ - For Chinese search.

------------
Installation
------------

With your virtualenv activated::

   cd src
   git clone https://gitlab.com/iislod/ckanext-iislod.git
   cd ckanext-iislod
   pip install -r requirements.txt
   python setup.py develop

-------------
Configuration
-------------

Add the following plugins to your CKAN config file (by default the config file is located at ``/etc/ckan/default/production.ini``), then restart your server::

    ckan.plugins = ... dc15_harvester agent_harvester project_harvester event_harvester r1_harvester iislod
