from rdflib.namespace import Namespace, DC, DCTERMS, RDF, RDFS, SKOS, FOAF, XSD

NAMESPACE_MAPPING = {
    unicode(DC): 'dc',
    unicode(DCTERMS): 'dct',
    unicode(RDF): 'rdf',
    unicode(RDFS): 'rdfs',
    unicode(SKOS): 'skos',
    unicode(FOAF): 'foaf',
    unicode(XSD): 'xsd',
    u'http://schema.org/': 'schema',
    u'http://www.w3.org/ns/prov#': 'prov',
    u'http://guava.iis.sinica.edu.tw/r4r/': 'r4r',
    u'http://data.odw.tw/record/': 'data',
    u'http://data.odw.tw/project/': 'project',
    u'http://data.odw.tw/agent/': 'agent',
    u'http://voc.odw.tw/ontology#': 'voc',
    u'http://creativecommons.org/ns#': 'cc',
    u'http://www.w3.org/ns/org#': 'org',
    u'http://www.wikidata.org/entity/': 'wde',
    u'http://www.w3.org/ns/dcat#': 'dcat',
    u'http://sws.geonames.org/': 'gns',
    u'http://www.w3.org/2006/time#': 'time',
    u'http://rs.tdwg.org/dwc/terms/': 'dwc',
    u'http://eol.org/pages/': 'eol',
    u'http://purl.org/NET/c4dm/event.owl#': 'event',
    u'http://data.odw.tw/event/': 'evt84',
    u'http://www.w3.org/2003/01/geo/wgs84_pos#': 'geo',
    u'http://www.geonames.org/ontology#': 'gn',
    u'http://data.odw.tw/r1/': 'r1',
    u'http://lod.taxonconcept.org/ontology/txn.owl#': 'txn'
}

for uri, prefix in NAMESPACE_MAPPING.iteritems():
    exec(prefix + ' = Namespace(uri)')
