# -*- coding: utf-8 -*-

import json

import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit

from ckanext.iislod import helpers
from ckanext.iislod import validators

agents = helpers.get_record_agent_mapping()
licenses = helpers.get_uri_license_mapping()
roles = helpers.get_uri_role_mapping()


class IislodPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IRoutes, inherit=True)
    plugins.implements(plugins.ITemplateHelpers)
    plugins.implements(plugins.IPackageController, inherit=True)
    plugins.implements(plugins.IFacets)
    plugins.implements(plugins.IValidators)

    # IConfigurer
    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'iislod')

    # IRoutes
    def before_map(self, map):
        for theme in helpers.get_facet_items('theme'):
            map.redirect('/record/%s' %
                         theme['name'], '/record?theme=%s' % theme['name'])

        map.redirect('/record/{name}.{ext}', '/dataset/{name}.{ext}')
        map.redirect('/agent/{name}.{ext}', '/dataset/{name}.{ext}')
        map.redirect('/project/{name}.{ext}', '/dataset/{name}.{ext}')
        map.redirect('/event/{name}.{ext}', '/dataset/{name}.{ext}')
        map.redirect('/article/{name}.{ext}', '/dataset/{name}.{ext}')
        map.redirect('/r1/{name}.{ext}', '/dataset/{name}.{ext}')

        map.redirect('/record/{date}-{name}', '/dataset/{name}.ttl')
        map.redirect('/agent/{date}-{name}', '/dataset/{name}.ttl')
        map.redirect('/project/{date}-{name}', '/dataset/{name}.ttl')
        map.redirect('/article/{date}-{name}', '/dataset/{name}.ttl')
        map.redirect('/event/{event}-{name}', '/event/doids')

        return map

    def after_map(self, map):
        map.connect('record', '/record')
        map.connect('r1', '/r1')
        map.connect('examples', '/examples',
                controller='ckanext.iislod.controller:ExamplesController',
                action='index')

        return map

    # ITemplateHelpers
    def get_helpers(self):
        function_names = (
            'get_uri_project_mapping',
            'get_uri_license_mapping',
            'get_uri_role_mapping',
            'get_uri_scopenote_mapping',
            'get_uri_from_prefixed_name',
            'get_prefixed_name_from_uri',
            'get_triple_count',
            'get_record_count',
            'get_recent_packages',
            'get_facet_items',
            'get_record_title',
            'group_list_of_dict',
            'get_time_from_dct_requires',
            'get_gns_ids_from_dct_requires',
            'get_gns_names_from_dct_requires'
        )

        return _get_module_functions(helpers, function_names)

    # IPackageController
    def before_index(self, data_dict):
        if data_dict['type'] == 'agent':
            # Role
            if data_dict.get('r4risPackagedWith'):
                r4r_isPackagedWith = json.loads(data_dict['r4risPackagedWith'])
                for k, v in r4r_isPackagedWith[0].iteritems():
                    for k2 in v:
                        for k3, v3 in k2.iteritems():
                            if k3 == 'http://www.w3.org/ns/org#classification':
                                for k4, v4 in v3.iteritems():
                                    for k5 in v4:
                                        for k6, v6 in k5.iteritems():
                                            if k6 == 'http://schema.org/roleName':
                                                data_dict['role'] = roles[v6]

        if data_dict['type'] == 'record':
            # Theme, license and agent
            data_dict['theme'] = data_dict['dcatthemeTaxonomy'].split('/')[-1]
            if data_dict.get('provwasStartedBy'):
                prov_wasStartedBy = json.loads(data_dict['provwasStartedBy'])
                for k, v in prov_wasStartedBy[0].iteritems():
                    for k2 in v:
                        for k3, v3 in k2.iteritems():
                            if k3 == 'http://creativecommons.org/ns#license':
                                for k4, v4 in v3.iteritems():
                                    data_dict['icon_license'] = licenses[k4]
                                    data_dict[
                                        'metadesc_license'] = licenses[k4]
                            if k3 == 'http://www.w3.org/ns/prov#value':
                                for k4, v4 in v3.iteritems():
                                    prov_value_revised = k4.replace(u'：', ':')
                                    if agents.get(prov_value_revised):
                                        data_dict['agent'] = agents[
                                            prov_value_revised]

        if data_dict['type'] == 'r1':
            # Theme
            data_dict['theme'] = data_dict['dcatthemeTaxonomy'].split('/')[-1]
            if data_dict.get('dctrequires'):
                dct_requires = json.loads(data_dict['dctrequires'])
                # Placename
                data_dict['placename'] = helpers.get_gns_names_from_dct_requires(dct_requires)
                times_from_dct_requires = helpers.get_time_from_dct_requires(dct_requires)
                # Time
                data_dict['point_time'] = times_from_dct_requires['point_time']
                data_dict['time_century'] = times_from_dct_requires['time_century']

        return data_dict

    # IFacets
    def dataset_facets(self, facets_dict, package_type):
        if package_type == 'record':
            facets_dict['agent'] = toolkit._('Agent')
            facets_dict['theme'] = toolkit._('Theme')
            facets_dict['metadesc_license'] = toolkit._('MetaDesc License')
            facets_dict['icon_license'] = toolkit._('ICON License')

        if package_type == 'r1':
            facets_dict['theme'] = toolkit._('Theme')
            facets_dict['time_century'] = toolkit._('Time in Century')
            facets_dict['placename'] = toolkit._('Placename')

        if package_type == 'agent':
            facets_dict['role'] = toolkit._('Role')

        return facets_dict


    def group_facets(self, facets_dict, group_type, package_type):

        return facets_dict


    def organization_facets(self, facets_dict, organization_type, package_type):

        return facets_dict

    # IValidators
    def get_validators(self):
        return {
            'repeating_mapping': validators.repeating_mapping
        }


def _get_module_functions(module, function_names):
    functions = {}
    for f in function_names:
        functions[f] = module.__dict__[f]

    return functions
