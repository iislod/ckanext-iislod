ckan.module('record-map', function (jQuery, _) {
  return {
    options: {
      table: '<table class="table table-striped table-bordered table-condensed"><tbody>{body}</tbody></table>',
      row: '<tr><th>{key}</th><td>{value}</td></tr>',
      gns_link_row: '<tr><th>{key}</th><td><a href="http://sws.geonames.org/{gns_id}" target="_blank">{name}</a></td></tr>',
      i18n: {
        'error': _('An error occurred: %(text)s %(error)s')
      }
    },
    initialize: function () {
      var self = this;

      self.el.empty();
      self.el.append($('<div></div>').attr('id', 'map'));
      self.map = L.map('map').setView([23.58, 120.58], 7);
      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors, <a href="http://www.geonames.org/export/">GeoNames</a>'
      }).addTo(self.map);

      self.markers = [];

      $(function () {
        jeoquery.defaultData.userName = 'u10313335';
      });

      self.showPreview(preload_geonames);
    },

    showError: function (jqXHR, textStatus, errorThrown) {
      if (textStatus == 'error' && jqXHR.responseText.length) {
        this.el.html(jqXHR.responseText);
      } else {
        this.el.html(this.i18n('error', {text: textStatus, error: errorThrown}));
      }
    },

    showPreview: function (gns_ids) {
      var self = this;

      self.map.spin(true);
      var fg = L.featureGroup([], {}).addTo(self.map);

      function dbg(data) {
        var marker = L.marker([data.lat, data.lng]);
        var body = '';
        body += L.Util.template(self.options.row, {key: 'geonameId', value: data.geonameId});
        body += L.Util.template(self.options.gns_link_row, {key: 'name', gns_id: data.geonameId, name: data.name});
        jQuery.each(data.alternateNames, function(index, value) {
          if (value.lang == 'zh') {
            body += L.Util.template(self.options.row, {key: 'name_zh', value: value.name});
          }
        });
        var popupContent = L.Util.template(self.options.table, {body: body});
        marker.bindPopup(popupContent);
        fg.addLayer(marker);
        self.map.fitBounds(fg.getBounds(), {maxZoom: 7});
        self.map.spin(false);
      }

      for(i=0; i<gns_ids.length; i++) {
        jeoquery.getGeoNames('get', { geonameId: gns_ids[i] }, dbg);
      }
    }
  }
});
