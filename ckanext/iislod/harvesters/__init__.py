from ckanext.iislod.harvesters.dc15 import DC15RDFHarvester
from ckanext.iislod.harvesters.agent import AgentRDFHarvester
from ckanext.iislod.harvesters.project import ProjectRDFHarvester
from ckanext.iislod.harvesters.event import EventRDFHarvester
from ckanext.iislod.harvesters.article import ArticleRDFHarvester
from ckanext.iislod.harvesters.r1 import R1RDFHarvester
