import json
import uuid
import logging

import ckan.plugins as p
import ckan.model as model
import ckan.logic as logic

from ckanext.harvest.model import HarvestObject, HarvestObjectExtra
from ckanext.dcat.processors import RDFParserException, RDFParser
from ckanext.dcat.interfaces import IDCATRDFHarvester
from ckanext.dcat.harvesters.rdf import DCATRDFHarvester

log = logging.getLogger(__name__)


class ArticleRDFHarvester(DCATRDFHarvester):

    MAX_FILE_SIZE = 1024 * 1024 * 200

    def info(self):
        return {
            'name': 'aritcle_harvester',
            'title': '84cc Article Harvester',
            'description': 'Harvester for 84cc LOD articles from an RDF graph'
        }

    def gather_stage(self, harvest_job):

        log.debug('In ArticleRDFHarvester gather_stage')

        # Get file contents
        url = harvest_job.source.url

        for harvester in p.PluginImplementations(IDCATRDFHarvester):
            url, before_download_errors = harvester.before_download(
                url, harvest_job)

            for error_msg in before_download_errors:
                self._save_gather_error(error_msg, harvest_job)

            if not url:
                return False

        rdf_format = None
        if harvest_job.source.config:
            rdf_format = json.loads(
                harvest_job.source.config).get("rdf_format")
        content, rdf_format = self._get_content_and_type(
            url, harvest_job, 1, content_type=rdf_format)

        # TODO: store content?
        for harvester in p.PluginImplementations(IDCATRDFHarvester):
            content, after_download_errors = harvester.after_download(
                content, harvest_job)

            for error_msg in after_download_errors:
                self._save_gather_error(error_msg, harvest_job)

        if not content:
            return False

        # TODO: profiles conf
        parser = RDFParser(profiles=['article_profile'])

        try:
            parser.parse(content, _format=rdf_format)
        except RDFParserException, e:
            self._save_gather_error(
                'Error parsing the RDF file: {0}'.format(e), harvest_job)
            return False

        guids_in_source = []
        object_ids = []
        for dataset in parser.datasets():
            if not dataset.get('name'):
                dataset['name'] = self._gen_new_name(dataset['title'])

            # Unless already set by the parser, get the owner organization (if any)
            # from the harvest source dataset
            if not dataset.get('owner_org'):
                source_dataset = model.Package.get(harvest_job.source.id)
                if source_dataset.owner_org:
                    dataset['owner_org'] = source_dataset.owner_org

            # Try to get a unique identifier for the harvested dataset
            guid = self._get_guid(dataset)

            if not guid:
                log.error(
                    'Could not get a unique identifier for dataset: {0}'.format(dataset))
                continue

            dataset['extras'].append({'key': 'guid', 'value': guid})
            guids_in_source.append(guid)

            obj = HarvestObject(guid=guid, job=harvest_job,
                                content=json.dumps(dataset))

            obj.save()
            object_ids.append(obj.id)

        # Check if some datasets need to be deleted
        object_ids_to_delete = self._mark_datasets_for_deletion(
            guids_in_source, harvest_job)

        object_ids.extend(object_ids_to_delete)

        return object_ids

    def fetch_stage(self, harvest_object):
        # Nothing to do here
        return True

    def import_stage(self, harvest_object):

        log.debug('In ArticleRDFHarvester import_stage')

        status = self._get_object_extra(harvest_object, 'status')
        if status == 'delete':
            # Delete package
            context = {'model': model, 'session': model.Session,
                       'user': self._get_user_name(), 'ignore_auth': True}

            p.toolkit.get_action('package_delete')(
                context, {'id': harvest_object.package_id})
            log.info('Deleted package {0} with guid {1}'.format(harvest_object.package_id,
                                                                harvest_object.guid))
            return True

        if harvest_object.content is None:
            self._save_object_error('Empty content for object {0}'.format(harvest_object.id),
                                    harvest_object, 'Import')
            return False

        try:
            dataset = json.loads(harvest_object.content)
        except ValueError:
            self._save_object_error('Could not parse content for object {0}'.format(harvest_object.id),
                                    harvest_object, 'Import')
            return False

        # Get the last harvested object (if any)
        previous_object = model.Session.query(HarvestObject) \
                                       .filter(HarvestObject.guid == harvest_object.guid) \
                                       .filter(HarvestObject.current == True) \
                                       .first()

        # Flag previous object as not current anymore
        if previous_object:
            previous_object.current = False
            previous_object.add()

        # Flag this object as the current one
        harvest_object.current = True
        harvest_object.add()

        context = {
            'user': self._get_user_name(),
            'return_id_only': True,
            'ignore_auth': True,
        }

        # Check if a dataset with the same guid exists
        existing_dataset = self._get_existing_dataset(harvest_object.guid)

        if existing_dataset:
            # Don't change the dataset name even if the title has
            dataset['name'] = existing_dataset['name']
            dataset['id'] = existing_dataset['id']

            # Save reference to the package on the object
            harvest_object.package_id = dataset['id']
            harvest_object.add()

            try:
                # TODO: Just a workaround. May be slow.
                new_dataset = p.toolkit.get_action(
                    'package_show')(context, {'id': dataset['id']})
                new_dataset.update(dataset)
                # For dicts in a list.
                for key, value in new_dataset.iteritems():
                    if key[:3] == 'dc:':
                        out = []
                        for element in value:
                            if isinstance(element, dict):
                                # Mapping
                                out.append(json.dumps(
                                    element, ensure_ascii=False))
                            else:
                                # String
                                out.append(element)
                        new_dataset[key] = out
                p.toolkit.get_action('package_update')(context, new_dataset)
            except p.toolkit.ValidationError, e:
                self._save_object_error('Update validation Error: %s' % str(
                    e.error_summary), harvest_object, 'Import')
                return False

            log.info('Updated dataset %s', dataset['name'])

        else:

            package_schema = logic.schema.default_create_package_schema()
            context['schema'] = package_schema

            # We need to explicitly provide a package ID
            dataset['id'] = unicode(uuid.uuid4())
            package_schema['id'] = [unicode]

            # Save reference to the package on the object
            harvest_object.package_id = dataset['id']
            harvest_object.add()

            # Defer constraints and flush so the dataset can be indexed with
            # the harvest object id (on the after_show hook from the harvester
            # plugin)
            model.Session.execute(
                'SET CONSTRAINTS harvest_object_package_id_fkey DEFERRED')
            model.Session.flush()

            try:
                p.toolkit.get_action('package_create')(context, dataset)
            except p.toolkit.ValidationError, e:
                self._save_object_error('Create validation Error: %s' % str(
                    e.error_summary), harvest_object, 'Import')
                return False

            log.info('Created dataset %s', dataset['name'])

        model.Session.commit()

        return True
