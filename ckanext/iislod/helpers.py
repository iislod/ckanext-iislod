# -*- coding: utf-8 -*-

import json
import requests
from pylons import config
from dateutil import parser
from datetime import datetime

import ckanapi

from ckanext.iislod import loader
from ckanext.iislod import namespaces as ns

AGENTS = loader.load_mapping(
    filename='agent.csv', key='record', value='abbreviation')
PROJECTS = loader.load_mapping(
    filename='project.csv', key='uri', value='project')
LICENSES = loader.load_mapping(
    filename='license.csv', key='uri', value='license')
ROLES = loader.load_mapping(
    filename='role.csv', key='uri', value='role')
SCOPENOTES = loader.load_mapping(
    filename='scopenote.csv', key='uri', value='scopeNote')
BIOLOGY_SPATIAL = loader.load_spatial_mapping(filename='內容主題-生物.dat')
GEOLOGY_SPATIAL = loader.load_spatial_mapping(filename='內容主題-地質.dat')
ARTIFACTS_SPATIAL = loader.load_spatial_mapping(filename='內容主題-器物.dat')
PAINTCAL_SPATIAL = loader.load_spatial_mapping(filename='內容主題-書畫.dat')
ARCHITECTURE_SPATIAL = loader.load_spatial_mapping(filename='內容主題-建築.dat')
STONERUB_SPATIAL = loader.load_spatial_mapping(filename='內容主題-拓片.dat')
ANTHROPOLOGY_SPATIAL = loader.load_spatial_mapping(filename='內容主題-人類學.dat')
MANUSCRIPT_SPATIAL = loader.load_spatial_mapping(filename='內容主題-善本古籍.dat')
ARCHIVES_SPATIAL = loader.load_spatial_mapping(filename='內容主題-檔案.dat')
NEWSMEDIA_SPATIAL = loader.load_spatial_mapping(filename='內容主題-新聞.dat')
MULTIMEDIA_SPATIAL = loader.load_spatial_mapping(filename='內容主題-影音.dat')
RESEARCHREUSE_SPATIAL = loader.load_spatial_mapping(
    filename='內容主題-學術社會應用推廣.dat')
ARCHAEOLOGY_SPATIAL = loader.load_spatial_mapping(filename='內容主題-考古.dat')

registry = ckanapi.LocalCKAN()


def prepare_sparql_query(graph_uri):
    string = {
        'query': 'select count(*) from ' + graph_uri + ' where {?s ?p ?o}',
        'format': 'application/json',
        'debug': 'off'}

    return string


def get_record_agent_mapping():

    return AGENTS


def get_uri_project_mapping():

    return PROJECTS


def get_uri_license_mapping():

    return LICENSES


def get_uri_role_mapping():

    return ROLES


def get_uri_scopenote_mapping():

    return SCOPENOTES


def get_oid_spatial_mapping():

    return {'Biology': BIOLOGY_SPATIAL,
            'Geology': GEOLOGY_SPATIAL,
            'Artifacts': ARTIFACTS_SPATIAL,
            'PaintCal': PAINTCAL_SPATIAL,
            'Architecture': ARCHITECTURE_SPATIAL,
            'StoneRub': STONERUB_SPATIAL,
            'Anthropology': ANTHROPOLOGY_SPATIAL,
            'Manuscript': MANUSCRIPT_SPATIAL,
            'Archives': ARCHIVES_SPATIAL,
            'NewsMedia': NEWSMEDIA_SPATIAL,
            'Multimedia': MULTIMEDIA_SPATIAL,
            'ResearchReuse': RESEARCHREUSE_SPATIAL,
            'Archaeology': ARCHAEOLOGY_SPATIAL}


def get_uri_from_prefixed_name(attr):
    [prefix, name] = attr.split(':')
    uri = eval('ns.%s' % (prefix))[name].toPython()

    return uri


def get_prefixed_name_from_uri(attr):
    for can_uri, prefix in ns.NAMESPACE_MAPPING.iteritems():
        if can_uri in attr:
            return prefix + ':' + attr.replace(can_uri, '')
    if SCOPENOTES.get(attr):
        return SCOPENOTES[attr]
    if LICENSES.get(attr):
        return LICENSES[attr]

    return None


def get_triple_count():
    counts = {}
    counts['dc15_count'] = 0
    counts['refined_count'] = 0

    endpoint_url = config.get('ckanext.sparql.endpoint_url', '')
    if not endpoint_url:
        return counts

    dc15_response = requests.get(
        endpoint_url, params=prepare_sparql_query('<urn:graph:iislod:record>'))
    if dc15_response.status_code == 200:
        dc15_response_dict = json.loads(dc15_response.text)
        counts['dc15_count'] = int(dc15_response_dict['results'][
                                   'bindings'][0]['callret-0']['value'])

    refined_response = requests.get(
        endpoint_url, params=prepare_sparql_query('<urn:graph:iislod:r1>'))
    if refined_response.status_code == 200:
        refined_response_dict = json.loads(refined_response.text)
        counts['refined_count'] = int(refined_response_dict['results'][
            'bindings'][0]['callret-0']['value'])

    return counts


def get_record_count():
    counts = {}
    counts['dc15_count'] = 0
    counts['refined_count'] = 0

    counts['dc15_count'] = registry.action.package_search(
        q='dataset_type:record', rows=1)['count']
    counts['refined_count'] = registry.action.package_search(
        q='dataset_type:r1', rows=1)['count']

    return counts


def get_recent_packages(limit=3):

    return registry.action.current_package_list_with_resources(limit=limit)


def get_facet_items(facet):

    return registry.call_action('package_search', {
        'q': '*:*',
        'facet.field': [facet],
        'rows': 4,
        'start': 0,
        'fq': 'capacity:"public"'
    })['search_facets'][facet]['items']


def get_record_title(r_name):

    record_title = ''
    record_name = 'd' + r_name[4:]

    try:
        record_title = registry.action.package_show(id=record_name)['title']
    except ckanapi.NotFound:
        return record_title

    return record_title


def group_list_of_dict(ld):
    out = {}

    if not isinstance(ld, list):
        return out

    for item in ld:
        k = item.keys()[0]
        if k not in out:
            out[k] = []
        v = item.values()[0]
        if isinstance(v, dict):
            v = v.keys()[0]
            if item[k][v]:
                v_id = v
                v = group_list_of_dict(item[k][v])
                v.update({'id': v_id})
        out[k].append(v)

    return out


def get_time_from_dct_requires(l):

    out_point_time = []
    out_time_century = []

    for v1 in l:
        for k2, v2 in v1.iteritems():
            for v3 in v2:
                for k4, v4 in v3.iteritems():
                    if get_prefixed_name_from_uri(k4) in ['dct:date', 'dwc:dateIdentified', 'dwc:eventDate', 'dwc:namePublishedInYear', 'schema:endDate', 'schema:startDate', 'schema:birthDate', 'schema:deathDate', 'voc:pointBefore', 'voc:pointAfter', 'voc:latest']:
                        for k5 in v4.keys():
                            formatted_time = parser.parse(
                                k5, default=datetime.min)
                            out_point_time.append(
                                formatted_time.isoformat() + 'Z')
                            formatted_time_century = formatted_time.year/100 + 1
                            out_time_century.append("%d Century" % (formatted_time_century - 1 if formatted_time.year%100 == 0 else formatted_time_century))

    return {'point_time': list(set(out_point_time)), 'time_century': list(set(out_time_century))}


def get_gns_ids_from_dct_requires(l):
    out = []

    for v1 in l:
        for k2, v2 in v1.iteritems():
            for v3 in v2:
                for k4, v4 in v3.iteritems():
                    if k4.split('#')[0] != 'http://www.geonames.org/ontology':
	                continue
                    out.append(v4.keys()[0].split('/')[-1])

    return out


def get_gns_names_from_dct_requires(l):
    out = []

    for v1 in l:
        for k2, v2 in v1.iteritems():
            for v3 in v2:
                for k4, v4 in v3.iteritems():
                    if k4.split('#')[0] != 'http://www.geonames.org/ontology':
                        continue
                    for k5, v5 in v4.iteritems():
                        for v6 in v5:
                            for k7, v7 in v6.iteritems():
                                if k7 != 'http://www.w3.org/2000/01/rdf-schema#label':
                                    continue
                                try:
                                    v7.encode('ascii')
                                    out.append(v7)
                                except:
                                    pass

    return list(set(out))
