import os
import inspect
import cPickle as pickle

import unicodecsv as csv


def load_mapping(filename, key, value):
    m = __import__('ckanext.iislod', fromlist=[''])
    mapping = dict()

    try:
        p = os.path.join(os.path.dirname(
            inspect.getfile(m)), 'assets/' + filename)
        reader = csv.DictReader(open(p, 'r'))
        for row in reader:
            mapping[row[key]] = row[value]
    except IOError:
        log.error('The mapping file: ' + filename + ' is missing.')

    return mapping


def load_spatial_mapping(filename):
    m = __import__('ckanext.iislod', fromlist=[''])
    try:
        p = os.path.join(os.path.dirname(
            inspect.getfile(m)), 'assets/spatial/' + filename)
        dat_file = open(p, 'rb')
        mapping = pickle.load(dat_file)
    except IOError:
        log.error('The spatial mapping file: ' + filename + ' is missing.')

    return mapping
