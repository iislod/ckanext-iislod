import json
from rdflib import URIRef, Literal

from ckan.lib import helpers as h
from ckanext.dcat.profiles import RDFProfile

from ckanext.iislod import helpers
from ckanext.iislod import namespaces as ns

namespaces = {
    'skos': ns.skos,
    'prov': ns.prov,
    'voc': ns.voc,
    'agent': ns.agent,
    'r4r': ns.r4r,
    'schema': ns.schema,
    'data': ns.data,
    'evt84': ns.evt84,
    'event': ns.event
}


class EventProfile(RDFProfile):
    '''
    An RDF profile for the events of Open Data Web
    '''

    def _get_list_value(self, value):
        items = []
        # List of values
        if isinstance(value, list):
            items = value
        elif isinstance(value, basestring):
            try:
                # JSON list
                items = json.loads(value)
            except ValueError:
                if ',' in value:
                    # Comma-separated list
                    items = value.split(',')
                else:
                    # Normal text value
                    items = [value]

        return items

    def parse_dataset(self, dataset_dict, dataset_ref):

        dataset_dict['tags'] = []
        dataset_dict['extras'] = []
        dataset_dict['resources'] = []
        dataset_dict['type'] = 'event'
        dataset_dict['notes'] = ''

        # Basic fields
        for key, predicate in (
                ('r4r:locateAt', ns.r4r.locateAt),
                ('skos:definition', ns.skos.definition),
                ('prov:startedAtTime', ns.prov.startedAtTime),
                ('prov:endedAtTime', ns.prov.endedAtTime),
                ('prov:atLocation', ns.prov.atLocation),
                ('r4r:isPackagedWith', ns.r4r.isPackagedWith),
                ('r4r:hasLicense', ns.r4r.hasLicense)
                ):
            value = self._object_value(dataset_ref, predicate)
            if value:
                dataset_dict[key] = value

        # Lists with 3-level nested triples
        for key, predicate in (
                ('event:sub_event', ns.event.sub_event),
                ('r4r:hasProvenance', ns.r4r.hasProvenance)
                ):
            value_nodes = self.g.objects(dataset_ref, predicate)
            if value_nodes:
                out = []
                for node in value_nodes:
                    inner = {node: []}
                    for inner_pred, inner_object in self.g.predicate_objects(node):
                        inner_2 = {inner_object: []}
                        for inner_pred_2, inner_object_2 in self.g.predicate_objects(inner_object):
                            inner_2[inner_object].append({inner_pred_2: inner_object_2})
                        inner[node].append({inner_pred: inner_2})
                    out.append(json.dumps(inner))
                if out: dataset_dict[key] = out

        #  Lists
        for key, predicate in (
                ('rdf:type_meta', ns.rdf['type']),
                ('rdf:type_prov', ns.rdf['type']),
                ('prov:wasAssociatedWith', ns.prov.wasAssociatedWith)
                ):
            values = self._object_value_list(dataset_ref, predicate)
            if values:
                if key == 'rdf:type_meta' and helpers.get_uri_from_prefixed_name('data:Provenance') in values:
                    continue
                if key == 'rdf:type_prov' and helpers.get_uri_from_prefixed_name('voc:Event') in values:
                    continue
                dataset_dict[key] = values

        # Name for CKAN
        dataset_dict['name'] = unicode(dataset_ref).split('/')[-1].split('-', 2)[-1].lower()

        # Title for CKAN
        dataset_dict['title'] = 'Event ID collection'

        return dataset_dict

    def graph_from_dataset(self, dataset_dict, dataset_ref):

        g = self.g

        meta_uri = dataset_dict['r4r:isPackagedWith']

        meta_ref = URIRef(meta_uri)

        for prefix, namespace in namespaces.iteritems():
            g.bind(prefix, namespace)

        # List of links with nested triples
        for key, predicate in (
                ('event:sub_event', ns.event.sub_event),
		('r4r:hasProvenance', ns.r4r.hasProvenance),
                ):
            value = self._get_dict_value(dataset_dict, key)
            if value:
                items = self._get_list_value(value)
                for item in items:
                    for k, v in item.iteritems():
                        inner_subject = URIRef(k) if h.is_url(k) else Literal(k, datatype=ns.rdf.PlainLiteral)
                        for v2 in v:
                            for k3, v3 in v2.iteritems():
                                inner_subject_2 = URIRef(k3) if h.is_url(k3) else Literal(k3, datatype=ns.rdf.PlainLiteral)
                                for k4, v4 in v3.iteritems():
                                    for v5 in v4:
                                        for k6, v6 in v5.iteritems():
                                            g.add((URIRef(k4), URIRef(k6), URIRef(v6) if h.is_url(v6) else Literal(v6)))
                                    k4_datatype = None
                                    k4_lang = None
                                    if helpers.get_prefixed_name_from_uri(inner_subject_2) in ['prov:startedAtTime', 'prov:endedAtTime']:
                                        k4_datatype = ns.xsd.DateTime
                                    if helpers.get_prefixed_name_from_uri(inner_subject_2) in ['skos:editorialNote']:
                                        k4_datatype = ns.rdf.PlainLiteral
                                    if helpers.get_prefixed_name_from_uri(inner_subject_2) in ['skos:scopeNote']:
                                        k4_lang = 'en'
                                    g.add((URIRef(inner_subject), inner_subject_2, URIRef(k4) if h.is_url(k4) else Literal(k4, datatype=k4_datatype, lang=k4_lang)))
                        g.add((meta_ref, predicate, inner_subject))
