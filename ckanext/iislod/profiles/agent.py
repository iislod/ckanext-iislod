import json

from rdflib import URIRef, Literal

from ckan.lib import helpers as h
from ckanext.dcat.profiles import RDFProfile

from ckanext.iislod import helpers
from ckanext.iislod import namespaces as ns

namespaces = {
    'skos': ns.skos,
    'wde': ns.wde,
    'prov': ns.prov,
    'voc': ns.voc,
    'agent': ns.agent,
    'r4r': ns.r4r,
    'schema': ns.schema,
    'cc': ns.cc,
    'org': ns.org,
    'dct': ns.dct,
    'data': ns.data
}


class AgentProfile(RDFProfile):
    '''
    An RDF profile for the agents of Open Data Web
    '''

    def _get_list_value(self, value):
        items = []
        # List of values
        if isinstance(value, list):
            items = value
        elif isinstance(value, basestring):
            try:
                # JSON list
                items = json.loads(value)
            except ValueError:
                if ',' in value:
                    # Comma-separated list
                    items = value.split(',')
                else:
                    # Normal text value
                    items = [value]

        return items

    def parse_dataset(self, dataset_dict, dataset_ref):

        dataset_dict['tags'] = []
        dataset_dict['extras'] = []
        dataset_dict['resources'] = []
        dataset_dict['type'] = 'agent'
        dataset_dict['notes'] = ''

        # Basic fields
        for key, predicate in (
                ('r4r:locateAt', ns.r4r.locateAt),
                ('r4r:hasProvenance', ns.r4r.hasProvenance),
                ('prov:startedAtTime', ns.prov.startedAtTime),
                ('prov:endedAtTime', ns.prov.endedAtTime),
                ('prov:atLocation', ns.prov.atLocation),
                ('r4r:hasLicense', ns.r4r.hasLicense)
        ):
            value = self._object_value(dataset_ref, predicate)
            if value:
                dataset_dict[key] = value

        # Lists with 3-level nested triples
        for key, predicate in (
                ('org:classification', ns.org.classification),
                ('r4r:isPackagedWith', ns.r4r.isPackagedWith)
        ):
            value_nodes = self.g.objects(dataset_ref, predicate)
            if value_nodes:
                out = []
                for node in value_nodes:
                    inner = {node: []}
                    for inner_pred, inner_object in self.g.predicate_objects(node):
                        inner_2 = {inner_object: []}
                        for inner_pred_2, inner_object_2 in self.g.predicate_objects(inner_object):
                            inner_2[inner_object].append(
                                {inner_pred_2: inner_object_2})
                        inner[node].append({inner_pred: inner_2})
                    out.append(json.dumps(inner))
                if out:
                    dataset_dict[key] = out

        #  Lists
        for key, predicate in (
                ('rdf:type_meta', ns.rdf['type']),
                ('rdf:type_prov', ns.rdf['type']),
                ('org:subOrganizationOf', ns.org.subOrganizationOf),
                ('org:memberOf', ns.org.memberOf),
                ('prov:wasAssociatedWith', ns.prov.wasAssociatedWith)
        ):
            values = self._object_value_list(dataset_ref, predicate)
            if values:
                if key == 'rdf:type_meta' and helpers.get_uri_from_prefixed_name('data:Provenance') in values:
                    continue
                if key == 'rdf:type_prov' and helpers.get_uri_from_prefixed_name('data:Agent') in values:
                    continue
                dataset_dict[key] = values

        # Lists of mappings
        for key, predicate in (
                ('rdfs:label', ns.rdfs.label),
        ):
            values = [o for o in self.g.objects(dataset_ref, predicate)]
            if values:
                out = []
                for value in values:
                    if isinstance(value, Literal):
                        k = value.language
                        v = value
                        out.append(json.dumps({k: v}))
                dataset_dict[key] = out

        # Name for CKAN
        dataset_dict['name'] = unicode(
            dataset_ref).split('/')[-1].split('-', 2)[-1]

        # Title for CKAN
        for o in self.g.objects(dataset_ref, ns.rdfs.label):
            if o.language == 'zh':
                dataset_dict['title'] = unicode(o)

        return dataset_dict

    def graph_from_dataset(self, dataset_dict, dataset_ref):

        g = self.g

        prov_uri = dataset_dict['r4r:hasProvenance']
        for key, value in dataset_dict['r4r:isPackagedWith'][0].iteritems():
            meta_uri = key

        meta_ref = URIRef(meta_uri)
        prov_ref = URIRef(prov_uri)

        for prefix, namespace in namespaces.iteritems():
            g.bind(prefix, namespace)

        # List of links with nested triples
        for key, predicate in (
                ('org:classification', ns.org.classification),
        ):
            value = self._get_dict_value(dataset_dict, key)
            if value:
                items = self._get_list_value(value)
                for item in items:
                    for k, v in item.iteritems():
                        inner_subject = URIRef(k) if h.is_url(
                            k) else Literal(k, datatype=ns.rdf.PlainLiteral)
                        for v2 in v:
                            for k3, v3 in v2.iteritems():
                                inner_subject_2 = URIRef(k3) if h.is_url(
                                    k3) else Literal(k3, datatype=ns.rdf.PlainLiteral)
                                for k4, v4 in v3.iteritems():
                                    for v5 in v4:
                                        for k6, v6 in v5.iteritems():
                                            v6_datatype = ns.rdf.PlainLiteral
                                            v6_lang = ''
                                            if helpers.get_prefixed_name_from_uri(k6) in ['prov:endedAtTime', 'prov:startedAtTime']:
                                                (v6_datatype, v6_lang) = (
                                                    ns.xsd.DateTime, None)
                                            if helpers.get_prefixed_name_from_uri(k6) in ['skos:editorialNote']:
                                                (v6_datatype, v6_lang) = (
                                                    None, 'en')
                                            g.add((URIRef(k4), URIRef(k6), URIRef(v6) if h.is_url(
                                                v6) else Literal(v6, datatype=v6_datatype, lang=v6_lang)))
                                    k4_datatype = None if helpers.get_prefixed_name_from_uri(
                                        inner_subject_2) == 'prov:value' else ns.rdf.PlainLiteral
                                    k4_lang = 'zh' if helpers.get_prefixed_name_from_uri(
                                        inner_subject_2) == 'prov:value' else ''
                                    g.add((URIRef(inner_subject), inner_subject_2, URIRef(
                                        k4) if h.is_url(k4) else Literal(k4)))
                        g.add((meta_ref, predicate, inner_subject))

        for key, predicate in (
                ('r4r:isPackagedWith', ns.r4r.isPackagedWith),
        ):
            value = self._get_dict_value(dataset_dict, key)
            if value:
                items = self._get_list_value(value)
                for item in items:
                    for k, v in item.iteritems():
                        inner_subject = URIRef(k) if h.is_url(
                            k) else Literal(k, datatype=ns.rdf.PlainLiteral)
                        for v2 in v:
                            for k3, v3 in v2.iteritems():
                                inner_subject_2 = URIRef(k3) if h.is_url(
                                    k3) else Literal(k3, datatype=ns.rdf.PlainLiteral)
                                for k4, v4 in v3.iteritems():
                                    for v5 in v4:
                                        for k6, v6 in v5.iteritems():
                                            g.add((URIRef(k4), URIRef(k6), URIRef(
                                                v6) if h.is_url(v6) else Literal(v6)))
                                    g.add((URIRef(inner_subject), inner_subject_2, URIRef(
                                        k4) if h.is_url(k4) else Literal(k4)))
                        g.add((prov_ref, predicate, inner_subject))
