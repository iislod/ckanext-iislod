import json

from rdflib import URIRef, Literal
from rdflib.term import _is_valid_uri

from ckan.lib import helpers as h
from ckanext.dcat.profiles import RDFProfile

from ckanext.iislod import helpers
from ckanext.iislod import namespaces as ns

namespaces = {
    'schema': ns.schema,
    'prov': ns.prov,
    'r4r': ns.r4r,
    'data': ns.data,
    'cc': ns.cc,
    'dct': ns.dct,
    'dcat': ns.dcat,
    'agent': ns.agent,
    'gns': ns.gns,
    'voc': ns.voc,
    'event': ns.event,
    'skos': ns.skos,
    'gn': ns.gn,
    'txn': ns.txn,
    'eol': ns.eol,
    'evt84': ns.evt84,
    'dwc': ns.dwc,
    'geo': ns.geo,
    'r1': ns.r1,
    'time': ns.time,
    'wde': ns.wde
}


class R1Profile(RDFProfile):
    '''
    An RDF profile for the data of Open Data Web
    '''

    def _get_list_value(self, value):
        items = []
        # List of values
        if isinstance(value, list):
            items = value
        elif isinstance(value, basestring):
            try:
                # JSON list
                items = json.loads(value)
            except ValueError:
                if ',' in value:
                    # Comma-separated list
                    items = value.split(',')
                else:
                    # Normal text value
                    items = [value]

        return items

    def _add_element_to_list_in_dict(self, d, key, l_value):
        if key not in d:
            d[key] = []
        d[key].append(l_value)

        return d

    def parse_dataset(self, dataset_dict, dataset_ref):

        dataset_dict['tags'] = []
        dataset_dict['extras'] = []
        dataset_dict['resources'] = []
        dataset_dict['type'] = 'r1'
        dataset_dict['notes'] = ''

        # Basic fields
        for key, predicate in (
                ('r4r:locateAt', ns.r4r.locateAt),
                ('dcat:landingPage', ns.dcat.landingPage),
                ('dcat:themeTaxonomy', ns.dcat.themeTaxonomy),
                ('r4r:hasProvenance', ns.r4r.hasProvenance),
                ('prov:wasInfluencedBy', ns.prov.wasInfluencedBy),
                ('prov:startedAtTime', ns.prov.startedAtTime),
                ('prov:endedAtTime', ns.prov.endedAtTime),
                ('prov:atLocation', ns.prov.atLocation)
        ):
            value = self._object_value(dataset_ref, predicate)
            if value:
                dataset_dict[key] = value

        #  Lists
        for key, predicate in (
                ('dct:date', ns.dct.date),
                ('schema:location', ns.schema.location),
                ('prov:wasAssociatedWith', ns.prov.wasAssociatedWith)
        ):
            values = self._object_value_list(dataset_ref, predicate)
            if values:
                dataset_dict[key] = values

        dataset_dict['rdf:type_meta'] = [
            unicode(ns.data.Refined), unicode(ns.r4r.Data), unicode(ns.dcat.Dataset)]
        dataset_dict['rdf:type_prov'] = [unicode(ns.data.Provenance), unicode(
            ns.r4r.Provenance), unicode(ns.prov.Activity)]

        # Lists with 3-level nested triples
        for key, predicate in (
                ('txn:hasEOLPage', ns.txn.hasEOLPage),
                ('dct:temporal', ns.dct.temporal),
                ('dct:spatial', ns.dct.spatial),
                ('dct:requires', ns.dct.requires),
                ('r4r:isPackagedWith', ns.r4r.isPackagedWith)
        ):
            value_nodes = self.g.objects(dataset_ref, predicate)
            if value_nodes:
                out = []
                for node in value_nodes:
                    inner = {node: []}
                    for inner_pred, inner_object in self.g.predicate_objects(node):
                        inner_2 = {inner_object: []}
                        for inner_pred_2, inner_object_2 in self.g.predicate_objects(inner_object):
                            inner_2[inner_object].append(
                                {inner_pred_2: inner_object_2})
                        inner[node].append({inner_pred: inner_2})
                    out.append(json.dumps(inner))
                if out:
                    dataset_dict[key] = out

        # Name and time (indexing) for CKAN when importing meta
        dataset_dict['name'] = unicode(dataset_ref).split(
            '/')[-1].split('-', 2)[-1].replace('d', 'r1-r')

        # Title for CKAN
        dataset_dict['title'] = dataset_dict['name']

        return dataset_dict

    def graph_from_dataset(self, dataset_dict, dataset_ref):

        g = self.g

        prov_uri = dataset_dict['r4r:hasProvenance']
        for key, value in dataset_dict['r4r:isPackagedWith'][0].iteritems():
            meta_uri = key

        meta_ref = URIRef(meta_uri)
        prov_ref = URIRef(prov_uri)

        for prefix, namespace in namespaces.iteritems():
            g.bind(prefix, namespace)

        # List of links with nested triples
        for key, predicate in (
                ('txn:hasEOLPage', ns.txn.hasEOLPage),
                ('dct:temporal', ns.dct.temporal),
                ('dct:spatial', ns.dct.spatial),
                ('dct:requires', ns.dct.requires)
        ):
            value = self._get_dict_value(dataset_dict, key)
            if value:
                items = self._get_list_value(value)
                for item in items:
                    for k, v in item.iteritems():
                        inner_subject = URIRef(
                            k) if h.is_url(k) else Literal(k)
                        for v2 in v:
                            for k3, v3 in v2.iteritems():
                                inner_subject_2 = URIRef(
                                    k3) if h.is_url(k3) else Literal(k3)
                                for k4, v4 in v3.iteritems():
                                    for v5 in v4:
                                        for k6, v6 in v5.iteritems():
                                            g.add((URIRef(k4), URIRef(k6), URIRef(
                                                v6) if h.is_url(v6) else Literal(v6)))
                                    g.add((URIRef(inner_subject), inner_subject_2, URIRef(
                                        k4) if h.is_url(k4) else Literal(k4)))
                        g.add((meta_ref, predicate, inner_subject))

        # List of links with nested triples
        for key, predicate in (
                ('r4r:isPackagedWith', ns.r4r.isPackagedWith),
        ):
            value = self._get_dict_value(dataset_dict, key)
            if value:
                items = self._get_list_value(value)
                for item in items:
                    for k, v in item.iteritems():
                        inner_subject = URIRef(
                            k) if h.is_url(k) else Literal(k)
                        for v2 in v:
                            for k3, v3 in v2.iteritems():
                                inner_subject_2 = URIRef(
                                    k3) if h.is_url(k3) else Literal(k3)
                                for k4, v4 in v3.iteritems():
                                    for v5 in v4:
                                        for k6, v6 in v5.iteritems():
                                            g.add((URIRef(k4), URIRef(k6), URIRef(
                                                v6) if h.is_url(v6) else Literal(v6)))
                                    g.add((URIRef(inner_subject), inner_subject_2, URIRef(
                                        k4) if h.is_url(k4) else Literal(k4)))
                        g.add((prov_ref, predicate, inner_subject))
