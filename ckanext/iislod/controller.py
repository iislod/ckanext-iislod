import ckan.plugins as p
from ckan.lib.base import BaseController


class ExamplesController(BaseController):

    def index(self):
        return p.toolkit.render('home/examples.html')
